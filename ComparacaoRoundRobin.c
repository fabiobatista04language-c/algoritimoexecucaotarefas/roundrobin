#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>


#define SIZE 1024
#define COMENT ';'
typedef struct
{
    int numberJob;
    int beginning;
    int duration;
} Job;


FILE *file;
Job *jobs,*jobRR, *jobFCFS;
char buf[SIZE], txt[SIZE],*fileRead, *fileWrite;
int jobQuantity, txtPosition = 0,processQuantity, jobsTotalRR=0;
bool toRead = true;

void read_valid(char character){
    if (character == COMENT)
        toRead = false;
    if (character == '\n')
        toRead = true;
}

void fill_jobs(){    
    jobs[jobQuantity];
    jobs = (Job*)malloc(sizeof(Job) * jobQuantity);
    jobFCFS = (Job*)malloc(sizeof(Job) * jobQuantity);
    file = fopen(fileWrite,"r");
    int a =0, b=0, c=0, i = 0;
    while(fscanf(file,"%i %i %i", &a, &b, &c) != -1 && i < jobQuantity){
        jobs[i].numberJob = a;    
        jobs[i].beginning = b;
        jobs[i].duration = c;
        jobFCFS[i].numberJob = a;    
        jobFCFS[i].beginning = b;
        jobFCFS[i].duration = c;
        i++;        
    }
    fclose(file);

    jobQuantity = i ;
    for( i = 0; i < jobQuantity; i++){
        int j = jobs[i].duration;
        while (j > 0)
        {
            jobsTotalRR++;
            j -= processQuantity;
        }        
    }
    jobRR = (Job*)malloc(sizeof(Job) * jobsTotalRR);
    
}

void format_new_file(char character){
    if (character != 'T' && character != ' ' && character != '\n'){
        if (character == ','){
            txt[txtPosition++] = ' ';
        }
        else{
            txt[txtPosition++] = character;
        }
    }
    if (character == '\n'){
        txt[txtPosition++] = '\n';
    }
}

void write_new_file(){
    file = fopen(fileWrite, "w");
    fwrite(txt, sizeof(char), strlen(txt), file);
    fclose(file);
}

void write(){
    int i = 0;
    if(file = fopen(fileRead, "r")){
        fread(buf, sizeof(char), SIZE, file);
        fclose(file);

        file = fopen(fileRead, "r");
        fscanf(file,"%d", &jobQuantity);
        
        fclose(file);       

        for (; buf[i] != 'T'; i++){}

        for (; i < sizeof(buf); i++){
            read_valid(buf[i]);
            if (toRead){
                format_new_file(buf[i]);
            }
        }
        write_new_file();  
    }else{
        printf("Falha ao ler o arquivo informado");
        exit;
    }
}

void round_robin(){
    int count=jobQuantity,current = 0, quantumCurrent = processQuantity, s = 0;

    while(count !=0){
        while(jobs[current].duration <= 0 ){ 
            if(current >= jobQuantity)
                current=0;
            current++;
            if(current >= jobQuantity)
                current = 0;        
        }
        jobRR[s].numberJob = jobs[current].numberJob;
        jobRR[s].duration = jobs[current].duration - processQuantity < 0 ? 1 : processQuantity;
        jobs[current].duration = jobs[current].duration - processQuantity;
        
        if( jobs[current].duration <=0 ){
            count--;
        }
        current++;
        if(current>=jobQuantity)
            current=0;
        s++;        
    }
    printf("Round Robin:\n");
}

void FCFS(){
    int count=0;
    while(count < jobQuantity){
        count++;
    }
    printf("FCFS:\n");

}

void print_time_line(Job p[], int n){
    int i, j,size;
    
    printf(" ");
    for(i=0; i<n; i++) {
        for(j=0; j<=p[i].duration-1; j++) printf("--");
        printf(" ");
    }
    printf("\n|");
    // middle position
    for(i=0; i<n; i++) {
        for(j=0; j<p[i].duration-1; j++) printf(" ");
        printf("T%i", p[i].numberJob);
        for(j=0; j<p[i].duration-1; j++) printf(" ");
        printf("|");
    }
    printf("\n ");
    // bottom bar
    for(i=0; i<n; i++) {
        for(j=0; j<=p[i].duration-1; j++) printf("--");
        printf(" ");
    }
}

void avarage_round_robin(Job p[]){
    printf("\n");
    int i,j, total = 0;
    for(i=0; i<jobsTotalRR; i++) {
        printf("%d", total);
        for(j=0; j<=p[i].duration-1; j++) printf("  ");
        total += p[i].duration;
    }
    printf("\n\n");
    printf("Tempo total de espera: %d\n", total);
    printf("Tempo Médio de espera: %.2f\n", (float)total/jobsTotalRR);
    printf("\n\n");
}

void avarage_FCFS(Job p[]){
    printf("\n");
    int i,j, total = 0;
    for(i=0; i<jobQuantity; i++) {
        printf("%d", total);
        for(j=0; j<=p[i].duration-1; j++) printf("  ");
        total += p[i].duration;
    }
    printf("\n\n");
    printf("Tempo total de espera: %d\n", total);
    printf("Tempo Médio de espera: %.2f\n", (float)total/jobQuantity);
    printf("\n\n");
}

int main(int argc, char *argv[]){
    if(argc < 4){
        printf("Ao executar o programa de ser informado como parametro n file1 file2\n");
        printf("n quantidade de processamento RR\n");
        printf("file1 nome do arquivo de leitura\n");
        printf("file2 nome do arquivo de saida\n");
        return 0;
    }
    processQuantity = atoi(argv[1]);
    fileRead = argv[2];
    fileWrite = argv[3];

    printf("\n\n");
        
    write();
    fill_jobs();

    round_robin();    
    print_time_line(jobRR, jobsTotalRR);
    avarage_round_robin(jobRR);

    FCFS();
    print_time_line(jobFCFS, jobQuantity);
    
    avarage_FCFS(jobFCFS);
    
    return 0;
}

